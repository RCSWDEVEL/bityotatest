﻿using UnityEngine;
using System.Collections;


public class HelloWorld : MonoBehaviour {

	public GUIStyle customButton;
	// Use this for initialization
	void Start () {
		BitYota.initialize("LGNTFKSJGBBSWDYQ");
		print("Started app with BitYota init.\n");
		print("Screen width " + Screen.width );
	}
	
	void OnGUI () {
		if (GUI.Button(new Rect(50, 70, Screen.width/2, Screen.width/4), "BitYota", customButton))
		{
			BeforeEvent("Before logging");
			Debug.Log("Clicked the button with BitYota");
			AfterEvent("After logging");
		}

		if (GUI.Button(new Rect(50, 70 + Screen.width/4 + 20, Screen.width/2, Screen.width/4), "Quit", customButton))
		{
			OnQuit();
		}
	}

	void BeforeEvent(string str) {
		string [] strs  = {"", "efg", "imn"};
		string [] keys  = {"bkey1", "bkey2", "bkey3"};
		strs[0] = str;

		print (str);
		BitYota.sendEvent( "ios_custom", strs , keys);
		BitYota.flushEvents();
	}

	void AfterEvent(string str) {
		string [] strs  = {"", "foo", "bar"};
		string [] keys  = {"akey1", "akey2", "akey3"};
		strs[0] = str;
		print (str);
		BitYota.sendEvent( "ios_custom", strs , keys);
		BitYota.flushEvents();
	}

	
	///<summary>
	///  User wants to quit
	///</summary>
	void OnQuit ()
	{
		Debug.Log ("Quiting game...");
#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;        
#else
		Application.Quit ();                
#endif
	}
}
