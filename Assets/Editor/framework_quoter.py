#!/usr/bin/python
import sys
import os
import re
from sys import argv

path = argv[1] + '/Unity-iPhone.xcodeproj/project.pbxproj'
pathw = '/tmp/Unity-iPhone.xcodeproj-project.pbxproj'
logf = open('/tmp/fqlog.txt', 'w')
srcf = open(path)
dstf = open(pathw, 'w')

logf.write('path = '+path+'\n')
logf.write('pathw = '+pathw+'\n')

p = re.compile(r'(\s)+"(\$\(SRCROOT.+)",')

for i, line in enumerate(srcf):
    m = p.search(line)
    if m:
        logf.write('('+str(i)+') : '+m.group(1)+'"\\"'+m.group(2)+'\\"",\n')
        dstf.write(m.group(1)+'"\\"'+m.group(2)+'\\"",\n')

    else:
        dstf.write(line)


dstf.close()
srcf.close()
logf.close()

os.rename(pathw, path)

