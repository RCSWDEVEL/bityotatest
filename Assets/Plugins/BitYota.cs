using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

/**
 * 		Changelog
 * 
 * bhales - December 19 2013
 * 
 * initializewithdictionary was flawed in design since it converted values
 * to strings in the dll but the BitYota library takes those values as
 * NSNumbers.  Without looking into the library, it is conceivable that
 * the standard "Value" methods are used in which case the strings would
 * be converted to numbers, but this is a lot of "what ifs" so I changed
 * the function name to apease my OCD desire to have mixed case as well
 * as change the parameters to an array of integers.
 * 
 * All of the event calling functions assumed that "C" could determine
 * the size of an array of pointers ... this isn't the case.  I added
 * a count field to these dll functions but this didn't change the
 * Unity side of the API since I CAN count sizes there.
 * 
 * Added error output if the count of objects and keys didn't match
 */

public class BitYota : MonoBehaviour 
{	
	// This method will be called for create instance of bityota
	[DllImport("__Internal")]
	private static extern void _BYinstance();
	
	public static void instance()
	{
		if (Application.platform == RuntimePlatform.IPhonePlayer)
			// INFINITE LOOP? instance();
						_BYinstance ();
	}
	
	// This method will be used for initialize bityota with appid
	[DllImport("__Internal")]
	private static extern void _BYinitialize(string appid);
	[DllImport("__Internal")]
	private static extern void _BYinitializeDebug(string appid);

	public static void initialize(string apptrackingid, bool debugging = false) {
			if (Application.platform == RuntimePlatform.IPhonePlayer) {
						if (debugging)
								_BYinitializeDebug (apptrackingid);
						else
								_BYinitialize (apptrackingid);	
				}
	}
	
	
	// This method will be used for initialize bityota with appid and extra params
	// Note that calling initialize with debugging set to true will enable debug
	// logging without needing to create the common key / value pairs
	// also note that dictionary values are int not string.
	[DllImport("__Internal")]
	private static extern void _BYinitializeWithDictionary(string appid, int[] values, string[] keys, int countof);
	
	public static void initializeWithDictionary(string appid, int[] values, string[] keys) {
		if (Application.platform == RuntimePlatform.IPhonePlayer) {
			int usecount = values.Length;
			if (usecount == keys.Length) {
				_BYinitializeWithDictionary(appid, values, keys, usecount);
			}

			else {
				Debug.LogError("BitYota: initializeWithDictionary count of values and keys didn't match.  Call was not issued to plug-in.");
			}
		}
	 }
	
	// This method will return id of eventTimer of bityota instance
	[DllImport("__Internal")]
	private static extern int _BYeventTimer();
		
	public static int eventTimer() {
		if(Application.platform == RuntimePlatform.IPhonePlayer)
			return _BYeventTimer();
		
		return 0;
	 }
	
	// This method will be used to sendEvent with type and objectdata and key data.
	[DllImport("__Internal")]
	private static extern void _BYsendEvent(string type, string[] values, string[] keys, int countof);
		
	public static void sendEvent(string type, string[] values, string[] keys)
	{
		if(Application.platform == RuntimePlatform.IPhonePlayer) {
			int usecount = values.Length;
			if (usecount == keys.Length) {
				_BYsendEvent(type, values, keys, usecount);
			}
			
			else {
				Debug.LogError("BitYota: sendEvent count of values and keys didn't match.  Call was not issued to plug-in.");
			}
		}
	 }
	
	// This method will be used to sendEventWithTimer with type and objectdata and key data and eventTimer
    [DllImport("__Internal")]
	private static extern void _BYsendEventWithTimer(string type, string[] values, string[] keys, int countof, string eventTimer);
	
	public static void sendEventWithTimer(string type, string[] values, string[] keys,string eventTimer)
	{
		if(Application.platform == RuntimePlatform.IPhonePlayer) {
			int usecount = values.Length;
			if (usecount == keys.Length) {
				_BYsendEventWithTimer(type, values, keys, usecount, eventTimer);
			}
			
			else {
				Debug.LogError("BitYota: sendEventWithTimer count of values and keys didn't match.  Call was not issued to plug-in.");
			}
		}
	}

	// This method will be used to flushEvents of Bityota instance
	[DllImport("__Internal")]
	private static extern void _BYflushEvents();
	
	public static void flushEvents()
	{
		if(Application.platform == RuntimePlatform.IPhonePlayer)
			_BYflushEvents();
	}
	
	// This method will clear event queue.
	[DllImport("__Internal")]
	private static extern void _BYclearEventQueue();
	
	public static void clearEventQueue()
	{
		if(Application.platform == RuntimePlatform.IPhonePlayer)
			_BYclearEventQueue();
	}
	
	// This method will be used for clear stats.
    [DllImport("__Internal")]
	private static extern void _BYclearStats();
	
    public static void clearStats()
	{
		if(Application.platform == RuntimePlatform.IPhonePlayer)
			_BYclearStats();
    } 
	
	// This method will be used for set enable stats.
    [DllImport("__Internal")]
    private static extern void _BYsetStatsEnables(bool state);
	
	public static void setStatsEnables(bool state)
	{
		if(Application.platform == RuntimePlatform.IPhonePlayer)
		_BYsetStatsEnables(state);
    }
	
	// This method will return built stat string.
	
	
	// This method will return event queue size.
    [DllImport("__Internal")]
	private static extern int _BYgetEventQueueSize();
	
	public static int getEventQueueSize()
	{
		if(Application.platform == RuntimePlatform.IPhonePlayer)
			return _BYgetEventQueueSize();
		
		return 0;
    }
	
	// This method will retrun flushing event.
	[DllImport("__Internal")]
	private static extern bool _BYgetIsFlushingEvents();
	
	public static bool getIsFlushingEvents()
	{
		if(Application.platform == RuntimePlatform.IPhonePlayer)
			return _BYgetIsFlushingEvents();
		
		return false;
    }
	
	//This method will be used to set api url.
    [DllImport("__Internal")]
	private static extern void _BYapiUrl(string api);
	
	public static void apiUrl(string api)
	{
		if(Application.platform == RuntimePlatform.IPhonePlayer)
			_BYapiUrl(api);
    }
	
	//This method will be used to set config sync period minutes.
    [DllImport("__Internal")]
	private static extern void _BYtcConfigSyncPeriodMins(int min);
	
	public static void tcConfigSyncPeriodMins(int min)
    {
		if(Application.platform == RuntimePlatform.IPhonePlayer)
			_BYtcConfigSyncPeriodMins(min);
    } 
	
	// This method will be used to set tracking code of bityota.
	[DllImport("__Internal")]
	private static extern void _BYtrackingCode(string track);
	
    public static void trackingCode(string tracknumber)
	{
		if(Application.platform == RuntimePlatform.IPhonePlayer)
			_BYtrackingCode(tracknumber);
	}
	
}